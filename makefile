#
# Output files
#
TARGET = ./mandelbrot

#
# Source
#
SRCS = src/main.c \
	   src/macosx/application.m \
	   src/gl_core_3_2.c

#
# Compilation control
#
INCLUDES 	+= -Isrc
DEFINES		+=
LIBS		+= -framework Cocoa -framework GLKit -framework OpenGL

C_STD	= -std=c89
CXX_STD	= -std=c++98
WARNINGS	+=	 -Wall -Wextra -pedantic -Wpointer-arith \
				 -Wwrite-strings  -Wredundant-decls -Winline -Wno-long-long \
				 -Wuninitialized -Wconversion -Werror
CPPFLAGS += -MMD -MP $(DEFINES) $(INCLUDES) $(WARNINGS) -g
CFLAGS += $(CPPFLAGS) -Wmissing-declarations -Wstrict-prototypes -Wnested-externs -Wmissing-prototypes $(C_STD)
CXXFLAGS += $(CPPFLAGS) $(CXX_STD)
LDFLAGS += $(LIBS)

#############################################
OBJECTS = $(patsubst %.cpp,%.o, \
		  $(patsubst %.c,%.o, \
		  $(patsubst %.m,%.o, $(SRCS))))
############################################

ifndef V
	SILENT = @
endif

_DEPS := $(OBJECTS:.o=.d) $(TEST_OBJECTS:.o=.d)

.PHONY: clean test external

all: $(TARGET)

$(TARGET) : $(OBJECTS) external
	@echo "Linking $@..."
	$(SILENT) $(CXX) $(LDFLAGS) $(OBJECTS) $(LIBRARY) -o $(TARGET)

%.o : %.m
	@echo "Compiling $<..."
	$(SILENT) $(CC) $(CFLAGS) -c $< -o $@

%.o : %.c
	@echo "Compiling $<..."
	$(SILENT) $(CC) $(CFLAGS) -c $< -o $@

%.o : %.cpp
	@echo "Compiling $<..."
	$(SILENT) $(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	@echo "Cleaning..."
	$(SILENT) $(RM) -f -r $(OBJECTS) $(_DEPS)
	$(SILENT) $(RM) $(TARGET)

-include $(_DEPS)

