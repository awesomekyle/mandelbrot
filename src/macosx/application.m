/** @file application.m
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include "gl_core_3_2.h"

#import <Cocoa/Cocoa.h>
#import <CoreVideo/CoreVideo.h>

#include "application.h"

/* Constants
 */
#define CheckGLError()                              \
    do {                                            \
        GLenum _glError = glGetError();             \
        if(_glError != GL_NO_ERROR) {               \
            fprintf(stderr, "OpenGL Error: %d\n", _glError); \
        }                                           \
        assert(_glError == GL_NO_ERROR);            \
    } while(0)


/* Types
 */

/** @brief Overloaded 'NSApplication' with a custom 'run' function
 */
@interface MacApplication : NSApplication
@end

/** @brief Controls the basic functionality of the app
 */
@interface AppDelegate : NSObject <NSApplicationDelegate>
@property(retain) NSWindow *window;
@end

/** @details This can be used directly or simply as an example. The `NSView`
 *   simply has to have a `pixelFormat` and `openGLContext`. `NSOpenGLView`
 *   defines these by default.
 */
@interface OpenGLView : NSView
@property(retain) NSOpenGLPixelFormat*  pixelFormat; /*!< The pixel format */
@property(retain) NSOpenGLContext*      openGLContext; /*!< The OpenGL context */
@end

/* Variables
 */

/* Internal functions
 */

@implementation OpenGLView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        /* Initialization code here. */
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,/* OpenGL 3.2 profile */
            NSOpenGLPFAAccelerated,                                 /* Only use hardware acceleration */
            NSOpenGLPFADoubleBuffer,                                /* Double buffer it */
            NSOpenGLPFAColorSize, (NSOpenGLPixelFormatAttribute)32, /* 32-bit color buffer */
            NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)24, /* 24-bit depth buffer */
            (NSOpenGLPixelFormatAttribute)nil,
        };
        GLint vsync = 0;

        /* Support high-resolution backing (retina) */
        [self setWantsBestResolutionOpenGLSurface:YES];

        /* Create the pixel format */
        [self setPixelFormat:[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes]];
        [self setOpenGLContext:[[NSOpenGLContext alloc] initWithFormat:[self pixelFormat] shareContext:nil]];
        [[self openGLContext] makeCurrentContext];

        /* Turn off VSync */
        [[self openGLContext] setValues:&vsync forParameter:NSOpenGLCPSwapInterval];

        assert(ogl_LoadFunctions() == ogl_LOAD_SUCCEEDED);
    }
    return self;
}
- (BOOL)acceptsFirstResponder
{
    return YES;
}
- (BOOL)preservesContentDuringLiveResize
{
    return YES;
}
- (void)drawRect:(NSRect)dirtyRect
{
    /* Drawing code here. */
    (void)sizeof(dirtyRect);
}
- (void)lockFocus
{
    NSOpenGLContext* context = [self openGLContext];

    [super lockFocus];
    if ([context view] != self) {
        [context setView:self];
    }
    [context makeCurrentContext];
}
- (void)update
{
    [[self openGLContext] update];
    CheckGLError();
}
- (void)reshape
{
    NSRect bounds = [self convertRectToBacking:[self bounds]];
    bounds.size.width = (int)bounds.size.width;
    bounds.size.height = (int)bounds.size.height;

    [[self openGLContext] makeCurrentContext];
    CheckGLError();
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CheckGLError();


    [self update];
    CheckGLError();
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    CheckGLError();
}
- (void)windowResized:(NSNotification *)notification
{
    [self reshape];
    (void)sizeof(notification);
}
- (void)viewDidMoveToWindow
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(windowResized:)
                                                 name:NSWindowDidResizeNotification
                                               object:[self window]];
}
- (void)toggleFullScreen:(id)sender
{
    [[self window] toggleFullScreen:sender];
}
@end
extern void _osx_flush_buffer(void* window);
void _osx_flush_buffer(void* window)
{
    NSOpenGLView* view = (NSOpenGLView*)[(__bridge NSWindow*)window contentView];
    [[view openGLContext] flushBuffer];
}

@implementation MacApplication

- (void)run
{
    _running = 1;
    [self finishLaunching];
    do {
        for(;;) {
            NSEvent* event = [self nextEventMatchingMask:NSAnyEventMask
                                               untilDate:[NSDate distantPast]
                                                  inMode:NSDefaultRunLoopMode
                                                 dequeue:YES];
            if(event == NULL)
                break;
            [self sendEvent:event];
            [self updateWindows];
        }

        /* Application per-frame code */
        if(on_frame())
            [self performSelector:@selector(terminate:) withObject:self];

    } while (_running);
}

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    int argc = (int)[arguments count];
    const char* argv[32] = { 0 };

    NSBundle* bundle = [NSBundle mainBundle];
    NSRect screenRect = [[NSScreen mainScreen] frame];
    unsigned int width = (unsigned int)(screenRect.size.width/2.0f);
    unsigned int height = (unsigned int)(screenRect.size.height/2.0f);
    NSRect frame = NSMakeRect(0, 0, width, height);
    int ii;

    [self setWindow:[[NSWindow alloc] initWithContentRect:frame
                                                styleMask:NSTitledWindowMask | NSResizableWindowMask
                                                  backing:NSBackingStoreBuffered
                                                    defer:YES]];
    [[self window] setFrameTopLeftPoint:NSMakePoint(0.0f, screenRect.size.height)];

    [[self window] setContentView:[[OpenGLView alloc] init]];
    [[self window] setOpaque:YES];
    [[self window] setAcceptsMouseMovedEvents:YES];
    [[self window] makeKeyAndOrderFront:nil];

    chdir([[[bundle bundlePath] stringByDeletingLastPathComponent] UTF8String]); /* Set cwd to Content/Resources */
    /* Get argc/argv */
    for(ii=0; ii<argc; ++ii)
    {
        NSString* arg = [arguments objectAtIndex:(NSUInteger)ii];
        argv[ii] = [arg UTF8String];
    }
    if(on_init(argc, argv))
        [NSApp performSelector:@selector(terminate:) withObject:self];

    (void)sizeof(aNotification);
}
-(NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    on_shutdown();
    return NSTerminateNow;
    (void)sizeof(sender);
}
@end

/* External functions
 */
void* system_window(void)
{
    return (__bridge void*)[[NSApp delegate] window];
}
int ApplicationMain(int argc, const char* argv[])
{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    Class principalClass = NSClassFromString([infoDictionary objectForKey:@"NSPrincipalClass"]);
    NSApplication* application = nil;
    NSString* mainNibName = nil;
    NSNib* mainNib = nil;


    if([principalClass respondsToSelector:@selector(sharedApplication)] == 0) {
        NSLog(@"NSPrincipalClass does not respond to `sharedApplication");
        return 1;
    }

    application = [principalClass sharedApplication];
    mainNibName = [infoDictionary objectForKey:@"NSMainNibFile"];
    mainNib = [[NSNib alloc] initWithNibNamed:mainNibName bundle:[NSBundle mainBundle]];
    [mainNib instantiateWithOwner:application topLevelObjects:nil];

    /* Start the loop */
    if([application respondsToSelector:@selector(run)]) {
        [application performSelectorOnMainThread:@selector(run) withObject:nil waitUntilDone:YES];
    }
    return 0;
    (void)sizeof(argc);
    (void)sizeof(argv[0]);
}
