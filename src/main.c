/** @file main.c
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <complex.h>
#include <math.h>
#include "gl_core_3_2.h"
#include "application.h"

/* Constants
 */
#define CheckGLError()                              \
    do {                                            \
        GLenum _glError = glGetError();             \
        if(_glError != GL_NO_ERROR) {               \
            fprintf(stderr, "OpenGL Error: %d\n", _glError); \
        }                                           \
        assert(_glError == GL_NO_ERROR);            \
    } while(0)
static const char* kVertexShaderSource =
    " #version 330  \n"\
    "   \n"\
    " layout(location=0) in vec4 in_Position;   \n"\
    " layout(location=1) in vec2 in_TexCoord;   \n"\
    "   \n"\
    " out vec2 int_TexCoord;    \n"\
    "   \n"\
    " void main()   \n"\
    " { \n"\
    "     vec4 pos = in_Position;   \n"\
    "     pos.x *= 2.0f, pos.y *= 2.0f;    \n"\
    "     gl_Position   = pos;   \n"\
    "     int_TexCoord  = in_TexCoord;    \n"\
    " }\n";
static const char* kFragmentShaderSource =
    "#version 330   \n"\
    "   \n"\
    "uniform sampler2D kTexture;\n"\
    "in vec2 int_TexCoord;  \n"\
    "   \n"\
    "out vec3 out_Color;    \n"\
    "   \n"\
    "void main()    \n"\
    "{  \n"\
    "    out_Color.rgb = texture(kTexture, vec2(int_TexCoord.x, -int_TexCoord.y)).rgb;  \n"\
    "}  \n";

static const int TEXTURE_WIDTH = 720*2;
static const int TEXTURE_HEIGHT = 450*2;

typedef struct {
    float pos[3];
    float tex[2];
} Vertex;

static const Vertex kVertices[] = {
    { { -0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f } },
    { {  0.5f, -0.5f, 0.0f }, { 1.0f, 1.0f } },
    { {  0.5f,  0.5f, 0.0f }, { 1.0f, 0.0f } },
    { { -0.5f,  0.5f, 0.0f }, { 0.0f, 0.0f } },
};
static const unsigned short kIndices[] =
{
    3,1,0,
    2,1,3,
};

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} Color;

/* Variables
 */
static GLuint _vao = 0;
static GLuint _vertex_buffer = 0;
static GLuint _index_buffer = 0;
static GLuint _program = 0;
static GLuint _texture = 0;

static Color*   _data = NULL;

/* Internal functions
 */
static GLuint _compile_shader(GLenum shader_type, const char* source)
{
    GLint status = GL_TRUE;
    GLuint shader = glCreateShader(shader_type);
    
    glShaderSource(shader, 1, &source, NULL);
    CheckGLError();
    glCompileShader(shader);
    CheckGLError();
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    CheckGLError();
    if(status == GL_FALSE) {
        char statusBuffer[1024*16];
        glGetShaderInfoLog(shader, sizeof(statusBuffer), NULL, statusBuffer);
        CheckGLError();
        fprintf(stderr, "Error compiling shader: %s", statusBuffer);
        glDeleteShader(shader);
        CheckGLError();
        shader = 0;
    }

    return shader;
}
static GLuint _create_program(GLuint vertex_shader, GLuint fragment_shader)
{
    GLint status = GL_TRUE;
    GLuint program = glCreateProgram();
    CheckGLError();
    glAttachShader(program, vertex_shader);
    CheckGLError();
    glAttachShader(program, fragment_shader);
    CheckGLError();
    glLinkProgram(program);
    CheckGLError();
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    CheckGLError();
    if(status == GL_FALSE) {
        char statusBuffer[1024*8];
        glGetProgramInfoLog(program, sizeof(statusBuffer), NULL, statusBuffer);
        CheckGLError();
        fprintf(stderr,"Link error: %s", statusBuffer);
        glDeleteProgram(program);
        CheckGLError();
        program = 0;
    }
    glUseProgram(0);
    return program;
}
static void _validate_program(GLuint program) {
    GLint status = GL_TRUE;
    glValidateProgram(program);
    CheckGLError();
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    CheckGLError();
    if(status == GL_FALSE) {
        char statusBuffer[1024*16];
        glGetProgramInfoLog(program, sizeof(statusBuffer), NULL, statusBuffer);
        CheckGLError();

        fprintf(stderr, "Link error: %s", statusBuffer);
    }
}
typedef struct {
    double r; 
    double g; 
    double b; 
} rgb;

    typedef struct {
    double h;       
    double s;       
    double v;       
} hsv;

    static hsv      rgb2hsv(rgb in);
    static rgb      hsv2rgb(hsv in);

static hsv rgb2hsv(rgb in)
{
    hsv         out;
    double      min, max, delta;

    min = in.r < in.g ? in.r : in.g;
    min = min  < in.b ? min  : in.b;

    max = in.r > in.g ? in.r : in.g;
    max = max  > in.b ? max  : in.b;

    out.v = max;                                
    delta = max - min;
    if( max > 0.0 ) {
        out.s = (delta / max);                  
    } else {                      
        out.s = 0.0;
        out.h = NAN;                            
        return out;
    }
    if( in.r >= max )                           
        out.h = ( in.g - in.b ) / delta;        
    else
    if( in.g >= max )
        out.h = 2.0 + ( in.b - in.r ) / delta;  
    else
        out.h = 4.0 + ( in.r - in.g ) / delta;  

    out.h *= 60.0;                              

    if( out.h < 0.0 )
        out.h += 360.0;

    return out;
}


static rgb hsv2rgb(hsv in)
{
    double      hh, p, q, t, ff;
    long        i;
    rgb         out;

    if(in.s <= 0.0) { 
        if(isnan(in.h)) { 
            out.r = in.v;
            out.g = in.v;
            out.b = in.v;
            return out;
        }
        out.r = 0.0;
        out.g = 0.0;
        out.b = 0.0;
        return out;
    }
    hh = in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.v * (1.0 - in.s);
    q = in.v * (1.0 - (in.s * ff));
    t = in.v * (1.0 - (in.s * (1.0 - ff)));

    switch(i) {
    case 0:
        out.r = in.v;
        out.g = t;
        out.b = p;
        break;
    case 1:
        out.r = q;
        out.g = in.v;
        out.b = p;
        break;
    case 2:
        out.r = p;
        out.g = in.v;
        out.b = t;
        break;

    case 3:
        out.r = p;
        out.g = q;
        out.b = in.v;
        break;
    case 4:
        out.r = t;
        out.g = p;
        out.b = in.v;
        break;
    case 5:
    default:
        out.r = in.v;
        out.g = p;
        out.b = q;
        break;
    }
    return out;     
}

static float hue2rgb(float p, float q, float t)
{
    if(t < 0.0f) t += 1.0f;
    if(t > 1.0f) t -= 1.0f;
    if(t < 1.0f/6.0f) return p + (q - p) * 6.0f * t;
    if(t < 1.0f/2.0f) return q;
    if(t < 2.0f/3.0f) return p + (q - p) * (2.0f/3.0f - t) * 6.0f;
    return p;
}
static Color _hsl_to_rgb(float h, float s, float l)
{
    hsv in = { h, s, l };
    rgb out = hsv2rgb(in);
    Color color;
    color.r = (uint8_t)(out.r*255.0f);
    color.g = (uint8_t)(out.g*255.0f);
    color.b = (uint8_t)(out.b*255.0f);
    return color;
}
/* External functions
 */
extern void _osx_flush_buffer(void* window);
int on_init(int argc, const char* argv[])
{
    /* Load shaders */
    GLuint vertex_shader = _compile_shader(GL_VERTEX_SHADER, kVertexShaderSource);
    GLuint fragment_shader = _compile_shader(GL_FRAGMENT_SHADER, kFragmentShaderSource);
    _program = _create_program(vertex_shader, fragment_shader);

    /* Set up geometry */
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);
    
    /* Create vertex buffer */
    glGenBuffers(1, &_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(kVertices), kVertices, GL_STATIC_DRAW);
    
    /* Create vertex buffer */
    glGenBuffers(1, &_index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(kIndices), kIndices, GL_STATIC_DRAW);

    /* Create texture */
    _data = calloc(1, sizeof(Color)*TEXTURE_WIDTH*TEXTURE_HEIGHT);
        
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEXTURE_WIDTH, TEXTURE_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    CheckGLError();

    /* Calculate mandelbrot */
    {
        float max = -1000.0f, min = 1000.0f;
        int xx,yy;
        for(xx=0;xx<TEXTURE_WIDTH;++xx) {
            for(yy=0;yy<TEXTURE_HEIGHT;++yy) {
                float x0 = (xx/(float)TEXTURE_WIDTH)*2.5f - 2.0f;
                float y0 = (yy/(float)TEXTURE_HEIGHT) * 2.0f - 1.0f;
                int iteration = 0;
                int max_iteration = 256;
                float smooth;
                float complex c, z;

                c = x0 + I * y0;
                z = 0.0f;

                while(cabsf(z) < 2.0f && iteration < max_iteration) {
                    z = z*z + c;
                    iteration++;
                }
                if(iteration != max_iteration) {
                    smooth = iteration+1 - log2f(cabsf(z))/logf(2.0f);
                    smooth /= max_iteration;
                    max = fmaxf(smooth, max);
                    min = fminf(smooth, min);
                    _data[yy*TEXTURE_WIDTH+xx] = _hsl_to_rgb(smooth, 0.1f, 0.5f);
                    _data[yy*TEXTURE_WIDTH+xx] = _hsl_to_rgb(smooth*360.0f, 0.6f, 1.0f);
                }
            }
        }
        printf("%f - %f\n", min, max);
    }
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, TEXTURE_WIDTH, TEXTURE_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, _data);

    
    glFrontFace(GL_CW);
    glDisable(GL_CULL_FACE);

    return 0;
    (void)sizeof(argc);
    (void)sizeof(*argv);
}
int on_frame(void)
{
    glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(_program);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)12);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);

    
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, TEXTURE_WIDTH, TEXTURE_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, _data);
    glBindTexture(GL_TEXTURE_2D, _texture);
    _validate_program(_program);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, NULL);
    CheckGLError();

    _osx_flush_buffer(system_window());
    return 0;
}
void on_shutdown(void)
{
    free(_data);
}
int main(int argc, const char* argv[])
{
    ApplicationMain(argc, argv);
    return 0;
}
