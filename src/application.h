/** @file application.h
 *  @brief Main application interface
 *  @copyright Copyright (c) 2013 Kyle Weicht. All rights reserved.
 */
#ifndef __application_h__
#define __application_h__

/** @brief Main entrypoint for the application
 *  @details This should be the only thing called from `main`. This is in charge
 *    of initializing the application for the specific OS/device. User code
 *    should all be placed in the various `on_XXXXX` handlers.
 *  @code
 *    int main(int argc, const char* argv[]) {
 *        return ApplicationMain(argc,argv);
 *    }
 *  @endcode
 */
int ApplicationMain(int argc, const char* argv[]);

/** User callbacks
 */
extern int on_init(int argc, const char* argv[]);
extern int on_frame(void);
extern void on_shutdown(void);

/** @brief Returns the applications window
 */
void* system_window(void);

#endif /* include guard */
